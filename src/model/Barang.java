/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Silvy Nur Azkia
 */
public class Barang {
   private String kode;
    private String nama;
    private int harga;

    public Barang(String kode, String nama, int harga) {
        this.kode = kode;
        this.nama = nama;
        this.harga = harga;
    }
 
    public String getKode() {
        return this.kode;
    }
 
    public void setKode(String kode) {
        this.kode = kode;
    }
 
    public String getNama() {
        return this.nama;
    }
 
    public void setNama(String nama) {
        this.nama = nama;
    }
 
    public int getHarga() {
        return this.harga;
    }
 
    public void setHarga(int harga) {
        this.harga = harga;
    }
 
    @Override
    public String toString() {
        return "{" +
                " kode='" + getKode() + "'" +
                ", nama='" + getNama() + "'" +
                ", harga='" + getHarga() + "'" +
                "}";
    }
 
}
