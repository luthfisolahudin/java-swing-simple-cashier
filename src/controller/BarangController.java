/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;
 
import model.Barang;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Silvy Nur Azkia
 */
public class BarangController {
    private final Connection connection;
 
    public BarangController(Connection connection) {
        this.connection = connection;
    }
 
    public void saveBrg(Barang barang) {
        String sql = "INSERT INTO bunga (id_bunga, nama_bunga, harga) VALUES (?,?,?)";
 
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, barang.getKode());
            statement.setString(2, barang.getNama());
            statement.setInt(3, barang.getHarga());
 
            statement.executeUpdate();
            System.out.println("Barang saved successfully.");
        } catch (SQLException e) {
        }
    }
 
    public List<Barang> getBrg() {
        List<Barang> barangList = new ArrayList<>();
        String sql = "SELECT * FROM bunga";
 
        try (PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                String id_bunga = resultSet.getString("id_bunga");
                String nama_bunga = resultSet.getString("nama_bunga");
                int harga = resultSet.getInt("harga");
 
                Barang barang = new Barang(id_bunga, nama_bunga, harga);
                barangList.add(barang);
            }
        } catch (SQLException e) {
        }
        return barangList;
    }
 
    public void updateBrg(Barang barangToUpdate) {
        String sql = "UPDATE bunga SET nama_bunga = ?, harga = ? WHERE id_bunga = ?";
 
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, barangToUpdate.getNama());
            statement.setInt(2, barangToUpdate.getHarga());
            statement.setString(3, barangToUpdate.getKode());
 
            int rowsUpdated = statement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Barang updated successfully.");
            } else {
                System.out.println("Failed to update barang.");
            }
        } catch (SQLException ignored) {
            //
        }
    }
}
